import os

config = {
    # Which I2C controller/interface/device to use
    "i2c_interface": os.environ.get("I2C_INTERFACE") or "/dev/i2c-1",

    # Where the wiringPi.so library is located
    "wiringpi_library_path": os.environ.get("WIRINGPI_LIBRARY_PATH") or "wiringPi.so",

    # Whether stub mode is active
    "testing_mode": bool(os.environ.get("TESTING_MODE"))
}
