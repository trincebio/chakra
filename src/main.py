from time import sleep
from i2c import I2CDevice

if __name__ == '__main__':
    print("Creating device")
    a = I2CDevice(0x62)
    while(1):
        print("Writing to device")
        sleep(1)
        a.write_multiple(b'R\r')
    a.close()
