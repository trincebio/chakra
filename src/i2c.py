from config import config
from wiringpi import wiringpi

# wiringPi will only be imported if the WiringPiI2CDevice class is used (see WiringPiI2CDevice.__init__)
class I2CException(Exception):
    pass


## This class represents a single instance of an I2C device.
class WiringPiI2CDevice:

    def __init__(self, address):
        print("Using I2C interface '%s'" % config["i2c_interface"])
        self.address = address
        self.handle = wiringpi.wiringPiI2CSetupInterface(bytes(config["i2c_interface"], encoding="utf8"), address)
        if (self.handle <= 0):
            raise I2CException("Could not open I2C device (got %d)" % self.handle)

    ## Writes multiple bytes to the I2C bus
    # @param values The values to write
    def write_multiple(self, values):
        for value in values:
            self.write(value)

    def write(self, value):
        # This function should return 0 if successful
        if (value > 255 or value < 0):
            raise I2CException("Value to write out of range (%d)" % value)
        if (wiringpi.wiringPiI2CWrite(self.handle, value)):
            raise I2CException("Could not write %d to device %d." % (value, self.address))

    def write_register(self, register, value):
        # This function should return 0 if successful
        if (value > 255 or value < 0):
            raise I2CException("Value to write out of range (%d)" % value)
        if (register > 255 or register < 0):
            raise I2CException("Register to write out of range (%d)" % register)
        if (wiringpi.wiringPiI2CWriteReg8(self.handle, register, value)):
            raise I2CException("Could not write %d to register %d in device %d." % (value, register, self.address))

    def read(self):
        # This function will return negative number if failed, otherwise the result
        result = wiringpi.wiringPiI2CRead(self.handle)
        if (result < 0):
            raise I2CException("Could not read from device %d" % self.address)
        return result

    def read_register(self, register):
        if (register > 255 or register < 0):
            raise I2CException("Register to read out of range (%d)" % register)
        # This function will return negative number if failed, otherwise the result
        result = wiringpi.wiringPiI2CReadReg8(self.handle, register)
        if (result < 0):
            raise I2CException("Could not read from device %d, register %d" % (self.address, register))
        return result

    def close(self):
        pass

class FakeI2CDevice:

    def __init__(self, address):
        global current_fake_id
        self.address = address
        self.registers = [0] * 256
        print("Creating fake I2C device (address %x)" % self.address)

    def write_multiple(self, values):
        for value in values:
            self.write(value)

    def write(self, value):
        if (value > 255 or value < 0):
            raise I2CException("Value to write out of range (%d)" % value)
        print("Writing to fake I2C device %x, write %d" % (self.address, value))

    def write_register(self, register, value):
        if (value > 255 or value < 0):
            raise I2CException("Value to write out of range (%d)" % value)
        if (register > 255 or register < 0):
            raise I2CException("Register to write out of range (%d)" % register)
        print("Writing to fake I2C device %x, write %d to register %d" % (self.address, value, register))
        self.registers[register] = value
        pass

    def read(self):
        value = 0
        print("Reading from fake I2C device %x, read %d" % (self.address, value))
        return value

    def read_register(self, register):
        if (register > 255 or register < 0):
            raise I2CException("Register to read out of range (%d)" % register)
        value = self.registers[register]
        print("Reading from fake I2C device %x, read %d from register %d" % (self.address, value, register))
        return value

    def close(self):
        pass

I2CDevice = FakeI2CDevice if config["testing_mode"] else WiringPiI2CDevice