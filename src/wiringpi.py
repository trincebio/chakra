import ctypes
from config import config

# This file contains the C bindings for the wiringPi library

if config["testing_mode"]:
    print("Not loading wiringPi because testing mode is active!")
    wiringpi = None
else:
    print("Loading wiringPi shared library from ", config["wiringpi_library_path"])

    # Load the wiringPi.so shared library file
    wiringpi = ctypes.CDLL(config["wiringpi_library_path"])
    wiringpi.wiringPiI2CSetupInterface.argtypes = [ctypes.c_char_p, ctypes.c_int]
    wiringpi.wiringPiI2CSetupInterface.restype = ctypes.c_int
    wiringpi.wiringPiI2CWrite.argtypes = [ctypes.c_int, ctypes.c_int]
    wiringpi.wiringPiI2CWrite.restype = ctypes.c_int
    wiringpi.wiringPiI2CWriteReg8.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_int]
    wiringpi.wiringPiI2CWriteReg8.restype = ctypes.c_int
    wiringpi.wiringPiI2CRead.argtypes = [ctypes.c_int]
    wiringpi.wiringPiI2CRead.restype = ctypes.c_int
    wiringpi.wiringPiI2CReadReg8.argtypes = [ctypes.c_int, ctypes.c_int]
    wiringpi.wiringPiI2CReadReg8.restype = ctypes.c_int
