# Chakra

This repository contains the code that will run on Tiffany and Chucky.

## Running

On embedded devices, you must specify the I2C interface to use (i2c controller device) and the path to the wiringPi shared library. Specify these using the following environment variables:

-   `WIRINGPI_LIBRARY_PATH`: path to the wiringPi shared library.
-   `I2C_INTERFACE`: path to the I2C controller (interface) to use.
-   (optional) `TESTING_MODE`: set this to 1 to activate testing mode. (Every device will be faked)

### Example: Chucky (TinkerBoard)

```sh
WIRINGPI_LIBRARY_PATH="/usr/lib/libwiringPi.so" I2C_INTERFACE="/dev/i2c-7" python3 main.py
```

### Example: Tiffany (Raspberry Pi)

```sh
WIRINGPI_LIBRARY_PATH="/usr/lib/libwiringPi.so" I2C_INTERFACE="/dev/i2c-1" python3 main.py
```

### Example: Development machine

Information will be printed to the console instead of the device.

```sh
TESTING_MODE="1" python3 main.py
```

## Generate documentation

Install doxygen and its requirements to generate the documentation.

```sh
sudo apt install doxygen texlive-latex-extra graphviz
```

To generate the documentation, enter the following command:

```
doxygen
```

This will create a latex output folder, which can then be converted into a pdf file using the following command:

```
cd latex && make refman.pdf
```

The refman.pdf is the output file.
