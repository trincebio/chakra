#!/bin/sh

# https://github.com/WiringPi/WiringPi
pi_build_wiring_pi() {
    echo "Building wiringPi..."

    rm -rf build/WiringPi
    mkdir --parents build/WiringPi

    # Create cross compiler
    docker run --rm dockcross/linux-armv7-lts > build/WiringPi/dockcross-linux-armv7-lts
    chmod u+x build/WiringPi/dockcross-linux-armv7-lts

    cd deps/WiringPi
    ../../build/WiringPi/dockcross-linux-armv7-lts bash -c 'cd wiringPi; make'
    cp wiringPi/libwiringPi.so.* ../../build/WiringPi/libwiringPi.so
    cd ../..
}

pi_build() {
    echo "Building for Tiffany (pi)..."

    rm -rf build/pi
    mkdir --parents build/pi

    # Create cross compiler
    docker run --rm dockcross/linux-armv7-lts > build/pi/dockcross-linux-armv7-lts
    chmod u+x build/pi/dockcross-linux-armv7-lts

    build/pi/dockcross-linux-armv7-lts bash -c '$CC -shared -Wl,-L./build/WiringPi,-lwiringPi -I./deps/WiringPi/wiringPi -I./src/include ./src/common/* ./src/hardware/* -o ./build/pi/chakra.so'

    echo "Done, build executable is build/pi/pi"
}

# https://github.com/TinkerBoard/gpio_lib_c
tinkerboard_build_wiring_pi() {
    echo "Building gpio_lib_c (tinkerboard port of wiringPi)..."

    rm -rf build/gpio_lib_c
    mkdir --parents build/gpio_lib_c

    # Create cross compiler
    docker run --rm dockcross/linux-arm64-lts > build/gpio_lib_c/dockcross-linux-arm64-lts
    chmod u+x build/gpio_lib_c/dockcross-linux-arm64-lts

    cd deps/gpio_lib_c
    ../../build/gpio_lib_c/dockcross-linux-arm64-lts bash -c 'cd wiringPi; make CC=$CC DEFS="-D_GNU_SOURCE -DTINKER_BOARD"'
    cp wiringPi/libwiringPi.so.* ../../build/gpio_lib_c/libwiringPi.so
    cd ../..
}

tinkerboard_build() {
    echo "Building Chakra for Chucky (tinkerboard)..."

    rm -rf build/tinkerboard
    mkdir --parents build/tinkerboard

    # Create cross compiler
    docker run --rm dockcross/linux-arm64-lts > build/tinkerboard/dockcross-linux-arm64-lts
    chmod u+x build/tinkerboard/dockcross-linux-arm64-lts

    build/tinkerboard/dockcross-linux-arm64-lts bash -c '$CC -shared -Wl,-L./build/gpio_lib_c,-lwiringPi -DTINKER_BOARD -I./deps/gpio_lib_c/wiringPi -I./src/include ./src/common/* ./src/hardware/* -o ./build/tinkerboard/chakra.so'

    echo "Done, build executable is build/tinkerboard/tinkerboard"
}

testing_build() {
    echo "Building testing application for current platform... (make sure gcc is installed!)"

    rm -rf build/testing
    mkdir --parents build/testing

    # Using installed gcc

    gcc -fPIC -shared -DTESTING -I./src/include ./src/common/*.c ./src/testing/*.c -o ./build/testing/chakra.so

    echo "Done, built executable is ./build/testing/chakra.so"
}

case $1 in

    testing)
        testing_build
        exit;;

    pi) 
        pi_build_wiring_pi
        pi_build
        exit;;

    tinkerboard) 
        tinkerboard_build_wiring_pi
        tinkerboard_build
        exit;;

    *)
        echo "Invalid build command '$1', please enter testing, pi or tinkerboard."
        exit;;

esac